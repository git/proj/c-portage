#include "internal.h"
#include "flag.h"

#include <string.h>

/**
 * Helper function to convert any type of Python string (Unicode or not) into a C string.
 * The object won't be DECREF'd but the returned char will be a copy of the data.
 * The Python Object can then be safely DECREF'd.
 */
static char* pyStringToString(PyObject *obj)
{
	if (!obj)
		return NULL;
	
	if (PyString_Check(obj))
	{
		return strdup(PyString_AsString(obj));
	}
	else if(PyUnicode_Check(obj))
	{
		PyObject *tmp = PyUnicode_AsUTF8String(obj);
		char *ret = strdup(PyString_AsString(tmp));
		Py_DECREF(tmp);
		return ret;
	}
	else if (PyBytes_Check(obj))
	{
		return strdup(PyBytes_AsString(obj));
	}

	return NULL;
}

StringList* portageGetIUse(const char* pkg)
{
	assert(pkg);
	PyObject *obj = executeFunction("portage.api.flag", "get_iuse", "(z)", pkg);
	if (!obj)
		return NULL;

	StringList *ret = listToCList(obj);

	Py_DECREF(obj);

	return ret;
}

StringList* portageGetInstalledUse(const char* pkg)
{
	assert(pkg);
	PyObject *obj = executeFunction("portage.api.flag", "get_installed_use", "(z)", pkg);
	if (!obj)
		return NULL;

	StringList *ret = listToCList(obj);

	Py_DECREF(obj);

	return ret;
}

StringList* portageGetInstalledPkgUse(const char* pkg)
{
	assert(pkg);
	PyObject *obj = executeFunction("portage.api.flag", "get_installed_use", "(zz)", pkg, "PKGUSE");
	if (!obj)
		return NULL;

	StringList *ret = listToCList(obj);

	Py_DECREF(obj);

	return ret;
}


char* portageReduceFlag(const char* flag)
{
	assert(flag);
	PyObject *obj = executeFunction("portage.api.flag", "reduce_flag", "(z)", flag);
	if (!obj)
		return NULL;

	char *ret = pyStringToString(obj);

	Py_DECREF(obj);

	return ret;
}


StringList* portageFilterFlags(StringList* use, StringList* use_expand_hidden, StringList* usemasked, StringList* useforced)
{
	PyObject *pyuse, *pyuseexp, *pyusemasked, *pyuseforced;
	pyuse = cListToPyList(use);
	pyuseexp = cListToPyList(use_expand_hidden);
	pyusemasked = cListToPyList(usemasked);
	pyuseforced = cListToPyList(useforced);
	PyObject *obj = executeFunction("portage.api.flag", "filter_flags", "(OOOO)", pyuse, pyuseexp, pyusemasked, pyuseforced);

	Py_DECREF(pyuse);
	Py_DECREF(pyuseexp);
	Py_DECREF(pyusemasked);
	Py_DECREF(pyuseforced);

	if (!obj)
		return NULL;

	StringList *ret = listToCList(obj);

	Py_DECREF(obj);

	return ret;
}


int portageGetAllCpvUse(const char* pkg, StringList** use, StringList** use_expand_hidden, StringList** usemasked, StringList** useforced)
{
	assert(pkg);
	assert(use);
	assert(use_expand_hidden);
	assert(usemasked);
	assert(useforced);

	PyObject *obj = executeFunction("portage.api.flag", "get_all_cpv_use", "(z)", pkg);
	if (!obj || !PySequence_Check(obj))
	{
		*use = stringListCreate(0);
		*use_expand_hidden = stringListCreate(0);
		*usemasked = stringListCreate(0);
		*useforced = stringListCreate(0);
		if (obj)
		{
			Py_DECREF(obj);
		}
		return 0;
	}

	PyObject *pyuse, *pyuseexp, *pyusemasked, *pyuseforced;
	pyuse = PySequence_GetItem(obj, 0);
	pyuseexp = PySequence_GetItem(obj, 1);
	pyusemasked = PySequence_GetItem(obj, 2);
	pyuseforced = PySequence_GetItem(obj, 3);

	*use = listToCList(pyuse);
	*use_expand_hidden = listToCList(pyuseexp);
	*usemasked = listToCList(pyusemasked);
	*useforced = listToCList(pyuseforced);

	Py_DECREF(pyuse);
	Py_DECREF(pyuseexp);
	Py_DECREF(pyusemasked);
	Py_DECREF(pyuseforced);
	Py_DECREF(obj);

	return 1;
}


StringList* portageGetFlags(const char* pkg)
{
	assert(pkg);
	PyObject *obj = executeFunction("portage.api.flag", "get_flags", "(z)", pkg);
	if (!obj)
		return NULL;

	StringList *ret = listToCList(obj);

	Py_DECREF(obj);

	return ret;
}

int portageGetFlagsFinal(const char* pkg, StringList** iuse_flags, StringList** final_flags)
{
	assert(pkg);
	PyObject *obj = executeFunction("portage.api.flag", "get_installed_use", "(zI)", pkg, 1);
	if (!obj || !PySequence_Check(obj))
	{
		*iuse_flags = stringListCreate(0);
		*final_flags = stringListCreate(0);
		if (obj)
		{
			Py_DECREF(obj);
		}
		return 0;
	}

	PyObject *pyiuse, *pyfinal;
	pyiuse = PySequence_GetItem(obj, 0);
	pyfinal = PySequence_GetItem(obj, 1);

	*iuse_flags = listToCList(pyiuse);
	*final_flags = listToCList(pyfinal);

	Py_DECREF(pyiuse);
	Py_DECREF(pyfinal);
	Py_DECREF(obj);

	return 1;
}


//int portageGetUseFlagDict()
//{
//}

