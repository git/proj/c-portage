#include "internal.h"
#include "portagesettings.h"

struct PortageSettings
{
	PyObject *object;
};

PortageSettings *portageSettingsCreate()
{
	PyObject *obj = executeFunction("portage.api.settings", "PortageSettings", NULL);

	// Call failed, library was not found.
	if (!obj)
		return NULL;
	
	PortageSettings *ret = malloc(sizeof(PortageSettings));
	ret->object = obj;

	return ret;
}

int portageSettingsResetUseFlags(PortageSettings *ps)
{
	assert(ps);
	PyObject *ret = PyObject_CallMethod(ps->object, "reset_use_flags", NULL);

	return ret != NULL;
}

int portageSettingsReset(PortageSettings *ps)
{
	assert(ps);
	PyObject *ret = PyObject_CallMethod(ps->object, "reset", NULL);

	return ret != NULL;
}

int portageSettingsReloadConfig(PortageSettings *ps)
{
	assert(ps);
	PyObject *ret = PyObject_CallMethod(ps->object, "reload_config", NULL);

	return ret != NULL;
}

int portageSettingsReloadWorld(PortageSettings *ps)
{
	assert(ps);
	PyObject *ret = PyObject_CallMethod(ps->object, "reload_world", NULL);

	return ret != NULL;
}

StringList* portageSettingsGetWorld(PortageSettings *ps)
{
	assert(ps);
	PyObject *obj = PyObject_CallMethod(ps->object, "get_world", NULL);

	if (!obj)
		return NULL;

	StringList *ret = listToCList(obj);
	Py_DECREF(obj);

	return ret;
}

StringList* portageSettingsGetArchList(PortageSettings *ps)
{
	assert(ps);
	PyObject *obj = PyObject_CallMethod(ps->object, "get_archlist", NULL);

	if (!obj)
		return NULL;

	StringList *ret = listToCList(obj);
	Py_DECREF(obj);

	return ret;
}

StringList* portageSettingsGetVirtuals(PortageSettings *ps)
{
	assert(ps);
	PyObject *obj = PyObject_CallMethod(ps->object, "get_virtuals", NULL);

	if (!obj)
		return NULL;

	StringList *ret = listToCList(obj);
	Py_DECREF(obj);

	return ret;
}

char* portageSettingsAcceptKeywords(PortageSettings *ps)
{
	assert(ps);
	PyObject *obj = PyObject_GetAttrString(ps->object, "ACCEPT_KEYWORDS");
	if (!obj)
	{
		return NULL;
	}

	if (!PyUnicode_Check(obj))
	{
		Py_DECREF(obj);
		return NULL;
	}

	PyObject *tmp = PyUnicode_AsUTF8String(obj);
	char *ret = strdup(PyString_AsString(tmp));
	
	Py_DECREF(tmp);
	Py_DECREF(obj);

	return ret;
}

StringList* portageSettingsSystemUseFlags(PortageSettings *ps)
{
	assert(ps);
	PyObject *obj = PyObject_GetAttrString(ps->object, "SystemUseFlags");
	if (!obj)
	{
		return NULL;
	}

	if (!PySequence_Check(obj))
	{
		Py_DECREF(obj);
		return NULL;
	}

	StringList *ret = listToCList(obj);

	Py_DECREF(obj);

	return ret;
}

/*void portageSettingsUseFlagDict();
{
	assert(ps);
	PyObject *obj = PyObject_GetAttrString(ps->object, "UseFlagDict");
	if (!obj)
	{
		return NULL;
	}

	if (!PyUnicode_Check(obj))
	{
		Py_DECREF(obj);
		return NULL;
	}

	PyObject *tmp = PyUnicode_AsUTF8String(obj);
	char *ret = strdup(PyString_AsString(tmp));
	
	Py_DECREF(tmp);
	Py_DECREF(obj);

	return ret;
}*/

char* portageSettingsArch(PortageSettings *ps)
{
	assert(ps);
	PyObject *obj = PyObject_GetAttrString(ps->object, "arch");
	if (!obj)
	{
		return NULL;
	}

	if (!PyUnicode_Check(obj))
	{
		Py_DECREF(obj);
		return NULL;
	}

	PyObject *tmp = PyUnicode_AsUTF8String(obj);
	char *ret = strdup(PyString_AsString(tmp));
	
	Py_DECREF(tmp);
	Py_DECREF(obj);

	return ret;
}

char* portageSettingsPortdir(PortageSettings *ps)
{
	assert(ps);
	PyObject *obj = PyObject_GetAttrString(ps->object, "portdir");
	if (!obj)
	{
		return NULL;
	}

	if (!PyUnicode_Check(obj))
	{
		Py_DECREF(obj);
		return NULL;
	}

	PyObject *tmp = PyUnicode_AsUTF8String(obj);
	char *ret = strdup(PyString_AsString(tmp));
	
	Py_DECREF(tmp);
	Py_DECREF(obj);

	return ret;
}

char* portageSettingsPortdirOverlay(PortageSettings *ps)
{
	assert(ps);
	PyObject *obj = PyObject_GetAttrString(ps->object, "portdir_overlay");
	if (!obj)
	{
		return NULL;
	}

	if (!PyUnicode_Check(obj))
	{
		Py_DECREF(obj);
		return NULL;
	}

	PyObject *tmp = PyUnicode_AsUTF8String(obj);
	char *ret = strdup(PyString_AsString(tmp));
	
	Py_DECREF(tmp);
	Py_DECREF(obj);

	return ret;
}

char* portageSettingsUserConfigDir(PortageSettings *ps)
{
	assert(ps);
	PyObject *obj = PyObject_GetAttrString(ps->object, "user_config_dir");
	if (!obj)
	{
		return NULL;
	}

	char *ret = NULL;

	if (PyUnicode_Check(obj))
	{
		PyObject *tmp = PyUnicode_AsUTF8String(obj);
		ret = strdup(PyString_AsString(tmp));
		Py_DECREF(tmp);
	}
	else if (PyString_Check(obj))
	{
		ret = PyString_AsString(obj);
	}
	
	
	Py_DECREF(obj);

	return ret;
}

void portageSettingsFree(PortageSettings* ps)
{
	if (ps && ps->object)
	{
		Py_DECREF(ps->object);
	}

	if (ps)
		free(ps);
}
