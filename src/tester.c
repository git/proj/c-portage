#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

#include "portage.h"

void printUseList(char* pkg)
{
	StringList *uses = portageGetFlags(pkg);
	StringList *iUses = portageGetInstalledUse(pkg);

	int count = 0;
	for (unsigned i = 0; i < stringListCount(uses); i++)
	{
		if (!stringListContains(iUses, stringListGetAt(uses, i)))
			count++;
	}
	
	StringList *tmp_list = stringListCreate(count + stringListCount(iUses));

	for (unsigned i = 0; i < stringListCount(iUses); i++)
	{
		stringListInsertAt(tmp_list, i, stringListGetAt(iUses, i));
	}

	int k = stringListCount(iUses);
	for (unsigned i = 0; i < stringListCount(uses); i++)
	{
		if (!stringListContains(iUses, stringListGetAt(uses, i)))
			stringListInsertAt(tmp_list, k++, stringListGetAt(uses, i));
	}

	for (unsigned i = 0; i < stringListCount(tmp_list); i++)
	{
		printf("%c%s%s", i < stringListCount(iUses) ? '+' : '-', stringListGetAt(tmp_list, i), i == stringListCount(tmp_list) - 1 ? "" : " ");
	}
	
	stringListFree(uses);
	stringListFree(iUses);
	stringListFree(tmp_list);
}

int main(int argc, char *argv[])
{
	int ret = 0;
	if (argc < 2)
	{
		printf("Please provide 1 package name.\n");
		return -1;
	}

	portageInit();

	char *pkg = argv[1];
	StringList *versions = portageGetVersions(pkg, 0);
	if (!versions)
	{
		printf("All masked?\n");
		return -1;
	}

	if (0 == stringListCount(versions))
	{
		stringListFree(versions);
		versions = portageGetVersions(pkg, 1);
		if (!versions)
		{
			printf("Bad ebuild ?\n");
			return -1;
		}

		if (0 == stringListCount(versions))
		{
			printf("No such ebuild '%s'\n", pkg);
			stringListFree(versions);
			return -1;
		}

		char *best_pkg = portageBestVersion(versions);
		assert(best_pkg);

		stringListFree(versions);

		StringList *m_status = portageGetMaskingStatus(best_pkg);

		for(unsigned int i = 0; i < stringListCount(m_status); i++)
		{
			if (strcmp(stringListGetAt(m_status, i), "") != 0)
			{
				printf("Package %s is masked :\n", best_pkg);
				char *m_reason = portageGetMaskingReason(best_pkg);
				printf("%s\n", m_reason);
				free(m_reason);
			}
		}

		stringListFree(m_status);
		free(best_pkg);
		return -1;
	}

	char *best_pkg = portageBestVersion(versions);
	assert(best_pkg);

	char *size = portageGetPackageSizeString(best_pkg);

	printf("%s USE=\"", best_pkg);
	printUseList(best_pkg);
	printf("\" %s\n", size);

	free(size);
	free(best_pkg);

	portageFinalize();

	return ret;
}
