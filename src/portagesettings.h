#ifndef PORTAGE_SETTINGS_H
#define PORTAGE_SETTINGS_H

#include "stringlist.h"

typedef struct PortageSettings PortageSettings;

PortageSettings *portageSettingsCreate();


// Reimplemented methods.
// Those are simple calls to the python methods.

int portageSettingsResetUseFlags(PortageSettings*);
int portageSettingsReset(PortageSettings*);
int portageSettingsReloadConfig(PortageSettings*);
int portageSettingsReloadWorld(PortageSettings*);
StringList* portageSettingsGetWorld(PortageSettings*);
StringList* portageSettingsGetArchList(PortageSettings*);
StringList* portageSettingsGetVirtuals(PortageSettings*);

// Fields access.
// Those are functions used to access different fields of the PortageSetting object.
//'ACCEPT_KEYWORDS', 'SystemUseFlags', 'UseFlagDict', 'arch', 'portdir', 'portdir_overlay', 'user_config_dir'
char* portageSettingsAcceptKeywords(PortageSettings*);
StringList* portageSettingsSystemUseFlags(PortageSettings*);
//void portageSettingsUseFlagDict();
char* portageSettingsArch(PortageSettings*);
char* portageSettingsPortdir(PortageSettings*);
char* portageSettingsPortdirOverlay(PortageSettings*);
char* portageSettingsUserConfigDir(PortageSettings*);


void portageSettingsFree(PortageSettings*);

#endif
