#ifndef PACKAGE_PROPERTIES
#define PACKAGE_PROPERTIES

#include "stringlist.h"

typedef struct PackageProperties PackageProperties;

PackageProperties* 	packagePropertiesCreate(char *slot, StringList* keywords, StringList* flags, StringList *homepages);

char* 			packagePropertiesGetSlot(PackageProperties *p);
StringList* 		packagePropertiesGetKeywords(PackageProperties *p);
StringList* 		packagePropertiesGetFlags(PackageProperties *p);
StringList* 		packagePropertiesGetHomepages(PackageProperties *p);

void			packagePropertiesFree(PackageProperties *p);

#endif
