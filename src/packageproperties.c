#include "stdlib.h"
#include "packageproperties.h"

struct PackageProperties
{
	char*		slot;
	StringList*	keywords;
	StringList*	flags;
	StringList*	homepages;
};

PackageProperties* packagePropertiesCreate(char *slot, StringList* keywords, StringList* flags, StringList *homepages)
{
	PackageProperties *ret = malloc(sizeof(PackageProperties));

	ret->slot = slot;
	ret->keywords = keywords;
	ret->flags = flags;
	ret->homepages = homepages;

	return ret;
}

char* packagePropertiesGetSlot(PackageProperties *p)
{
	return p->slot;
}

StringList* packagePropertiesGetKeywords(PackageProperties *p)
{
	return p->keywords;
}

StringList* packagePropertiesGetFlags(PackageProperties *p)
{
	return p->flags;
}

StringList* packagePropertiesGetHomepages(PackageProperties *p)
{
	return p->homepages;
}

void packagePropertiesFree(PackageProperties *p)
{
	free(p->slot);
	stringListFree(p->keywords);
	stringListFree(p->flags);
	stringListFree(p->homepages);

	free(p);
}
