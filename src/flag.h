#ifndef FLAG_H
#define FLAG_H

#include "stringlist.h"

StringList* portageGetIUse(const char*);
StringList* portageGetInstalledUse(const char*);
StringList* portageGetInstalledPkgUse(const char*);

char* portageReduceFlag(const char*);

StringList* portageFilterFlags(StringList*, StringList*, StringList*, StringList*);

int portageGetAllCpvUse(const char*, StringList**, StringList**, StringList**, StringList**);

StringList* portageGetFlags(const char*);
int portageGetFlagsFinal(const char*, StringList**, StringList**);

//int portageGetUseFlagDict();

#endif
