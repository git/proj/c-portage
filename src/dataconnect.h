#ifndef DATACONNECT_H
#define DATACONNECT_H

#include "stringlist.h"
#include "packageproperties.h"

StringList*		portageGetVersions(const char*, int);
int			portageGetHardMasked(const char*, StringList**, StringList**);
StringList*		portageGetInstalledFiles(const char*);

char*			portageBestVersion(StringList*);
char*			portageGetBestEbuild(const char*);
char*			portageGetDepEbuild(const char*);

StringList*		portageGetMaskingStatus(const char*);
char*			portageGetMaskingReason(const char*);

long int		portageGetPackageSizeInt(const char*);
char*			portageGetPackageSizeString(const char*);
PackageProperties*	portageGetProperties(const char*);
int			portageIsOverlay(const char*);
char*			portageGetOverlay(const char*);
char*			portageGetOverlayNameFromPath(const char*);
char*			portageGetOverlayNameFromPkg(const char*);
char*			portageGetPath(const char*, int);

StringList*		portageGetResolvedPkgs();
StringList*		portageGetUnresolvedPkgs();
StringList*		portageGetAllNodes();
StringList*		portageGetInstalledList();

#endif
