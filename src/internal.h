#ifndef INTERNAL_H
#define INTERNAL_H

#include <Python.h>

#include "stringlist.h"
#include "dict.h"

PyObject*	executeFunction(const char *module, const char *funcName, const char* format, ...);

StringList*	listToCList(PyObject* list);
PyObject*	cListToPyList(StringList*);

PyObject*	dictToPyDict(Dict *dict);

#endif
