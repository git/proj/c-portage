#include "internal.h"
#include "dataconnect.h"

/**
 * Helper function to convert anytype of Python string (Unicode or not) into a C string.
 * The object won't be DECREF'd but the returned char will be a copy of the data so it
 * can then be safely DECREF'd after the call.
 */
static char* pyStringToString(PyObject *obj)
{
	if (!obj)
		return NULL;
	
	if (PyString_Check(obj))
	{
		return strdup(PyString_AsString(obj));
	}
	else if(PyUnicode_Check(obj))
	{
		PyObject *tmp = PyUnicode_AsUTF8String(obj);
		char *ret = strdup(PyString_AsString(tmp));
		Py_DECREF(tmp);
		return ret;
	}
	else if (PyBytes_Check(obj))
	{
		return strdup(PyBytes_AsString(obj));
	}

	return NULL;
}

/**
 * All returned values, except for int, must be freed by the application, the library won't manage that.
 */

StringList* portageGetVersions(const char *pkg, int include_masked)
{
	PyObject *obj = executeFunction("portage.api.data_connect", "get_versions", "(zI)", pkg, include_masked);
	if (!obj || !PySequence_Check(obj))
	{
		if (obj)
		{
			Py_DECREF(obj);
		}
		return NULL;
	}

	StringList *ret = listToCList(obj);

	Py_DECREF(obj);

	return ret;
}

int portageGetHardMasked(const char* pkg, StringList** no_check, StringList** check)
{
	PyObject *obj = executeFunction("portage.api.data_connect", "get_hard_masked", "(z)", pkg);
	if (!obj || !PySequence_Check(obj))
	{
		if (obj)
		{
			Py_DECREF(obj);
		}
		return 0;
	}

	PyObject *pynocheck = PySequence_GetItem(obj, 0);
	assert(pynocheck);
	PyObject *pycheck = PySequence_GetItem(obj, 1);
	assert(pycheck);

	*no_check = listToCList(pynocheck);
	*check = listToCList(pycheck);

	Py_DECREF(pynocheck);
	Py_DECREF(pycheck);
	Py_DECREF(obj);

	return 1;
}

StringList* portageGetInstalledFiles(const char *pkg)
{
	PyObject *obj = executeFunction("portage.api.data_connect", "get_installed_files", "(z)", pkg);
	if (!obj || !PySequence_Check(obj))
	{
		if (obj)
		{
			Py_DECREF(obj);
		}
		return NULL;
	}

	StringList *ret = listToCList(obj);

	Py_DECREF(obj);

	return ret;
}


char* portageBestVersion(StringList *pkgs)
{
	assert(pkgs);
	PyObject *pylist = cListToPyList(pkgs);
	PyObject *obj = executeFunction("portage.api.data_connect", "best", "(O)", pylist);
	Py_DECREF(pylist);
	if (!obj)
		return NULL;

	char *ret = pyStringToString(obj);

	Py_DECREF(obj);

	return ret;
}

char* portageGetBestEbuild(const char *pkg)
{
	assert(pkg);
	PyObject *obj = executeFunction("portage.api.data_connect", "get_best_ebuild", "(z)", pkg);
	if (!obj)
	{
		if (obj)
		{
			Py_DECREF(obj);
		}
		return NULL;
	}

	char *ret = pyStringToString(obj);
	
	Py_DECREF(obj);

	return ret;
}

//FIXME:what does it return exactly ?
char* portageGetDepEbuild(const char *pkg)
{
	assert(pkg);
	PyObject *obj = executeFunction("portage.api.data_connect", "get_dep_ebuild", "(z)", pkg);
	if (!obj)
		return NULL;

	char *ret = pyStringToString(obj);

	Py_DECREF(obj);

	return ret;
}


//FIXME:are values always strings/unicodes ?
StringList* portageGetMaskingStatus(const char *pkg)
{
	assert(pkg);
	PyObject *obj = executeFunction("portage.api.data_connect", "get_masking_status", "(z)", pkg);
	if (!obj || !PySequence_Check(obj))
	{
		if (obj)
		{
			Py_DECREF(obj);
		}
		return NULL;
	}

	StringList *ret = listToCList(obj);

	Py_DECREF(obj);

	return ret;
}

char* portageGetMaskingReason(const char *pkg)
{
	assert(pkg);
	PyObject *obj = executeFunction("portage.api.data_connect", "get_masking_reason", "(z)", pkg);
	if (!obj)
		return NULL;

	char *ret = pyStringToString(obj);
	
	Py_DECREF(obj);

	return ret;
}


long int portageGetPackageSizeInt(const char *pkg)
{
	assert(pkg);
	PyObject *obj = executeFunction("portage.api.data_connect", "get_size", "(zI)", pkg, 0);
	if (!obj || !PyLong_Check(obj))
	{
		if (obj)
		{
			Py_DECREF(obj);
		}
		return 0;
	}

	long int ret = PyLong_AsLong(obj);

	Py_DECREF(obj);

	return ret;
}

char* portageGetPackageSizeString(const char *pkg)
{
	assert(pkg);
	PyObject *obj = executeFunction("portage.api.data_connect", "get_size", "(z)", pkg);
	if (!obj)
		return NULL;

	char *ret = pyStringToString(obj);

	Py_DECREF(obj);

	return ret;
}

PackageProperties* portageGetProperties(const char* pkg)
{
	assert(pkg);
	PyObject *obj = executeFunction("portage.api.data_connect", "get_properties", "(z)", pkg);
	if (!obj)
		return NULL;

	PyObject *slot = PyObject_CallMethod(obj, "get_slot", NULL);
	assert(slot);
	PyObject *keywords = PyObject_CallMethod(obj, "get_keywords", NULL);
	assert(keywords);
	PyObject *flags = PyObject_CallMethod(obj, "get_flags", NULL);
	assert(flags);
	PyObject *homepages = PyObject_CallMethod(obj, "get_homepages", NULL);
	assert(homepages);

	PackageProperties *ret = packagePropertiesCreate(pyStringToString(slot), listToCList(keywords), listToCList(flags), listToCList(homepages));

	return ret;
}

int portageIsOverlay(const char *pkg)
{
	assert(pkg);
	PyObject *obj = executeFunction("portage.api.data_connect", "is_overlay", "(z)", pkg);
	if (!obj)
		return 0;

	int ret = PyObject_IsTrue(obj);

	Py_DECREF(obj);

	return ret;
}

char* portageGetOverlay(const char *pkg)
{
	assert(pkg);
	PyObject *obj = executeFunction("portage.api.data_connect", "get_overlay", "(z)", pkg);
	if (!obj)
		return NULL;

	char *ret = pyStringToString(obj);
	
	Py_DECREF(obj);

	return ret;
}

char* portageGetOverlayNameFromPath(const char *path)
{
	assert(path);
	PyObject *obj = executeFunction("portage.api.data_connect", "get_overlay_name", "(z)", path);
	if (!obj || !PyUnicode_Check(obj))
	{
		if (obj)
		{
			Py_DECREF(obj);
		}
		return NULL;
	}

	char *ret = pyStringToString(obj);

	Py_DECREF(obj);

	return ret;
}

char* portageGetOverlayNameFromPkg(const char *pkg)
{
	assert(pkg);
	PyObject *none = Py_None;
	Py_INCREF(none);
	PyObject *obj = executeFunction("portage.api.data_connect", "get_overlay_name", "(Oz)", none, pkg);
	Py_DECREF(none);
	if (!obj)
		return NULL;

	char *ret = pyStringToString(obj);

	Py_DECREF(obj);

	return ret;
}

char* portageGetPath(const char* pkg, int vardb)
{
	assert(pkg);
	PyObject *obj = executeFunction("portage.api.data_connect", "get_path", "(zI)", pkg, vardb);
	if (!obj)
		return NULL;

	char *ret = pyStringToString(obj);

	Py_DECREF(obj);

	return ret;
}


StringList* portageGetResolvedPkgs()
{
	PyObject *obj = executeFunction("portage.api.data_connect", "get_system_pkgs", NULL);
	if (!obj || !PySequence_Check(obj))
	{
		if (obj)
		{
			Py_DECREF(obj);
		}
		return NULL;
	}

	PyObject *resolved = PySequence_GetItem(obj, 0);
	if (!resolved)
		return NULL;

	StringList *ret = listToCList(resolved);

	Py_DECREF(resolved);
	Py_DECREF(obj);

	return ret;
}

StringList* portageGetUnresolvedPkgs()
{
	PyObject *obj = executeFunction("portage.api.data_connect", "get_system_pkgs", NULL);
	if (!obj || !PySequence_Check(obj))
	{
		if (obj)
		{
			Py_DECREF(obj);
		}
		return NULL;
	}

	PyObject *unresolved = PySequence_GetItem(obj, 1);
	if (!unresolved)
		return NULL;

	StringList *ret = listToCList(unresolved);

	Py_DECREF(unresolved);
	Py_DECREF(obj);

	return ret;
}

StringList* portageGetAllNodes()
{
	PyObject *obj = executeFunction("portage.api.data_connect", "get_allnodes", NULL);
	if (!obj || !PySequence_Check(obj))
	{
		if (obj)
		{
			Py_DECREF(obj);
		}
		return NULL;
	}

	StringList *ret = listToCList(obj);

	Py_DECREF(obj);

	return ret;
}

StringList* portageGetInstalledList()
{
	PyObject *obj = executeFunction("portage.api.data_connect", "get_installed_list", NULL);
	if (!obj || !PySequence_Check(obj))
	{
		if (obj)
		{
			Py_DECREF(obj);
		}
		return NULL;
	}

	StringList *ret = listToCList(obj);

	Py_DECREF(obj);

	return ret;
}
