/*
    <one line to give the library's name and an idea of what it does.>
    Copyright (C) 2011  Θεόφιλος Ιντζόγλου <int.teo@gmail.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/


#include "qportage.h"
#include "qlistconverter.h"

extern "C" {
#include <dataconnect.h>
}

namespace CppPortage {

portage::portage(QObject* parent): QObject(parent)
{

}

portage::~portage()
{

}

QString portage::bestVersion(QStringList )
{

}

QStringList portage::allNodes()
{
    StringList *clist;
    QStringList strlist;
    
    clist = portageGetAllNodes();
    strlist = QListConverter::StringListToQStringList(clist);
    stringListFree(clist);
    
    return strlist;
}

QString portage::bestEbuild(const QString& )
{

}

QString portage::depEbuild(const QString& )
{

}

int portage::hardMasked(const QString& , QStringList& , QStringList& )
{

}

QStringList portage::installedFiles(const QString &package)
{
    StringList *clist;
    QStringList strlist;
    
    clist = portageGetInstalledFiles(package.toUtf8().data());
    strlist = QListConverter::StringListToQStringList(clist);
    stringListFree(clist);
    
    return strlist;
}

QStringList portage::installedList()
{
    StringList *clist;
    QStringList strlist;

    clist = portageGetInstalledList();
    strlist = QListConverter::StringListToQStringList(clist);
    stringListFree(clist);
    
    return strlist;
}

QString portage::maskingReason(const QString& )
{

}

QStringList portage::maskingStatus(const QString& )
{

}

QString portage::overlay(const QString& )
{

}

QString portage::overlayNameFromPath(const QString& )
{

}

QString portage::overlayNameFromPkg(const QString& )
{

}

long int portage::packageSizeInt(const QString& )
{

}

QString portage::packageSizeString(const QString& )
{

}

QString portage::path(const QString &package, int )
{

}

PackageProperties* portage::properties(const QString &package)
{
    return portageGetProperties(package.toUtf8().data());
}

QStringList portage::resolvedPkgs()
{
    StringList *clist;
    QStringList strlist;
    
    clist = portageGetResolvedPkgs();
    strlist = QListConverter::StringListToQStringList(clist);
    stringListFree(clist);
    
    return strlist;
}

QStringList portage::unresolvedPkgs()
{
    StringList *clist;
    QStringList strlist;
    
    clist = portageGetUnresolvedPkgs();
    strlist = QListConverter::StringListToQStringList(clist);
    stringListFree(clist);
    
    return strlist;
}

QStringList portage::versions(const QString &package, bool include_masked)
{
    StringList *clist = portageGetVersions(package.toUtf8().data(), include_masked);
    QStringList strlist = QListConverter::StringListToQStringList(clist);
    stringListFree(clist);
    
    return strlist;
}

bool portage::isOverlay(const QString &package)
{
    return portageIsOverlay(package.toUtf8().data());
}


} // End of namespace