#include <QCoreApplication>
#include <QDebug>
#include <QStringList>

#include "../qpackageproperties.h"
#include "../qlistconverter.h"

using namespace CppPortage;

void test();

void test() {

    QPackageProperties packPro(QString("slot"),QStringList("keyw"),QStringList("home"),QStringList("flags"));

    qDebug() << "QString Slot()" << packPro.Slot();
    qDebug() << "QStringList Homepages()" << packPro.Homepages();
    qDebug() << "QStringList Flags()" << packPro.Flags();
    qDebug() << "QStringList Keywords" << packPro.Keywords();
}

int main(int argc, char *argv[])
{
    test();
    QCoreApplication app(argc, argv);
    return app.exec();
}