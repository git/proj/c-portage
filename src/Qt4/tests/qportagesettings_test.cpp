#include <QCoreApplication>
#include <QDebug>
#include <QStringList>

#include "../qportagesettings.h"
#include "../qlistconverter.h"

using namespace CppPortage;

void test() {
    QPortageSettings portSett;

    qDebug() << "int resetUseFlags()" << portSett.resetUseFlags();
    qDebug() << "int reset()" << portSett.reset();
    qDebug() << "int reloadConfig()" << portSett.reloadConfig();
    qDebug() << "int reloadWorld()" << portSett.reloadWorld();
    qDebug() << "QStringList world()" << portSett.world();
    qDebug() << "QStringList archList()" << portSett.archList();
    qDebug() << "QStringList virtuals()" << portSett.virtuals();
    qDebug() << "QString acceptKeywords()" << portSett.acceptKeywords();
    qDebug() << "QStringList systemUseFlags()" << portSett.systemUseFlags();
    qDebug() << "QString arch()" << portSett.arch();
    qDebug() << "QString portdir()" << portSett.portdir();
    qDebug() << "QString portdirOverlay()" << portSett.portdirOverlay();
    qDebug() << "QString userConfigDir()" << portSett.userConfigDir();
}

int main(int argc, char *argv[])
{
    test();
    QCoreApplication app(argc, argv);

    return app.exec();
}
