#ifndef QLISTCONVERTER_H
#define QLISTCONVERTER_H

extern "C" {
#include "../stringlist.h"
}

#include <QObject>

class QString;
class QStringList;

namespace CppPortage {

class QListConverter : public QObject
{
    Q_OBJECT
public:
    QListConverter(QObject *parent = 0);

    /**
    * Converts a StringList to a QStringList
    **/
    static QStringList StringListToQStringList(StringList *stringList);

    /**
    * Converts a QStringList to a StringList
    **/
    static StringList* QStringListToStringList(QStringList qstringList);
};
}
#endif