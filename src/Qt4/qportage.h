/*
    <one line to give the library's name and an idea of what it does.>
    Copyright (C) 2011  Θεόφιλος Ιντζόγλου <int.teo@gmail.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/


#ifndef QPORTAGE_H
#define QPORTAGE_H

#include <QObject>
#include <QStringList>

class PackageProperties;

namespace CppPortage {

class portage : public QObject
{
    Q_OBJECT
    
    explicit portage(QObject* parent = 0);
    virtual ~portage();

    QStringList             versions(const QString&, bool);
    int                     hardMasked(const QString&, QStringList&, QStringList&);
    QStringList             installedFiles(const QString&);
    
    QString                 bestVersion(QStringList);
    QString                 bestEbuild(const QString&);
    QString                 depEbuild(const QString&);
    
    QStringList             maskingStatus(const QString&);
    QString                 maskingReason(const QString&);
    
    long int                packageSizeInt(const QString&);
    QString                 packageSizeString(const QString&);
    PackageProperties*      properties(const QString&);
    bool                    isOverlay(const QString&);
    QString                 overlay(const QString&);
    QString                 overlayNameFromPath(const QString&);
    QString                 overlayNameFromPkg(const QString&);
    QString                 path(const QString&, int);
    
    QStringList             resolvedPkgs();
    QStringList             unresolvedPkgs();
    QStringList             allNodes();
    QStringList             installedList();

}; // End of class

} // End of namespace

#endif // QPORTAGE_H
