#ifndef QPORTAGESETTINGS_H
#define QPORTAGESETTINGS_H


#include <QObject>
#include "qlistconverter.h"

extern "C" {
#include "../portagesettings.h"
}

class QString;
class QStringList;

struct PortageSettings;

namespace CppPortage {

class QPortageSettings : public QObject
{
    Q_OBJECT
public:
    QPortageSettings(QObject *parent = 0);
    virtual ~QPortageSettings();

    const int resetUseFlags();
    const int reset();
    const int reloadConfig();
    const int reloadWorld();
    const QStringList world();
    const QStringList archList();
    const QStringList virtuals();
    const QString acceptKeywords();
    const QStringList systemUseFlags();
    const QString arch();
    const QString portdir();
    const QString portdirOverlay();
    const QString userConfigDir();
};
}
#endif