#include "qlistconverter.h"

#include <QStringList>
#include <QString>

struct StringList
{
    char **list;
    unsigned int count;
};

namespace CppPortage{

QListConverter::QListConverter(QObject *parent)
        :QObject(parent)
{}

StringList* QListConverter::QStringListToStringList(QStringList qstringList)
{
    StringList *stringList = stringListCreate(qstringList.size());//create a new StringList

    for (int i = 0; i <= qstringList.size(); i++) {
        char c = *(qstringList.at(i).toUtf8().data());
        stringListInsertAt(stringList, i, &c);
    }

   return stringList;
}

QStringList QListConverter::StringListToQStringList(StringList *stringList)
{
    QStringList qstringList;

    for (int i = 0; i <= stringList->count; i++) {
        qstringList.insert(i, stringListGetAt(stringList, i));
    }

    return qstringList;
}
}//end namespace