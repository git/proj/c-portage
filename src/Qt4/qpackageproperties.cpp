#include "qpackageproperties.h"
#include "qlistconverter.h"

#include <QStringList>
#include <QString>

namespace CppPortage{

QPackageProperties::QPackageProperties(QString slot, QStringList keywords,
                                       QStringList flags, QStringList homepages, QObject *parent)
        :QObject(parent)
{
    StringList *FLAGS = QListConverter::QStringListToStringList(flags);

    StringList *HOMEPAGES = QListConverter::QStringListToStringList(homepages);

    StringList *KEYWORDS = QListConverter::QStringListToStringList(keywords);

    packagePropertiesCreate(slot.toUtf8().data(), KEYWORDS, FLAGS, HOMEPAGES);
}


QPackageProperties::~QPackageProperties()
{
    PackageProperties *p;
    void Free(PackageProperties *p);
}

const QString QPackageProperties::Slot()
{
    PackageProperties *p;
    return packagePropertiesGetSlot(p);
}

const QStringList QPackageProperties::Flags()
{
    PackageProperties *p;
    return QListConverter::StringListToQStringList(packagePropertiesGetFlags(p));
}

const QStringList QPackageProperties::Keywords()
{
    PackageProperties *p;
    return QListConverter::StringListToQStringList(packagePropertiesGetKeywords(p));
}

const QStringList QPackageProperties::Homepages()
{
    PackageProperties *p;    
    return QListConverter::StringListToQStringList(packagePropertiesGetHomepages(p));
}
}
