#ifndef QPACKAGEPROPERTIES_H
#define QPACKAGEPROPERTIES_H

#include <QObject>

extern "C" {
#include "../packageproperties.h"
}


class QString;
class QStringList;

struct PackageProperties;
struct StringList;

namespace CppPortage {

class QListConverter;

class QPackageProperties : public QObject
{
    Q_OBJECT
public:
    QPackageProperties(QString slot, const QStringList keywords, const QStringList flags,
                       QStringList homepages,QObject *parent = 0);
    virtual ~QPackageProperties();

    const QString Slot();
    const QStringList Keywords();
    const QStringList Flags();
    const QStringList Homepages();
};
}
#endif