#include "qportagesettings.h"

#include <QStringList>
#include <QString>

namespace CppPortage{

QPortageSettings::QPortageSettings(QObject *parent)
        :QObject(parent)
{
}

QPortageSettings::~QPortageSettings()
{
    PortageSettings *ps;
    portageSettingsFree(ps);
}

const int QPortageSettings::resetUseFlags()
{
    PortageSettings *ps;
    return portageSettingsResetUseFlags(ps);
}

const int QPortageSettings::reset()
{
    PortageSettings *ps;
    return portageSettingsResetUseFlags(ps);
}

const int QPortageSettings::reloadConfig()
{
    PortageSettings *ps;
    return portageSettingsReloadConfig(ps);
}

const int QPortageSettings::reloadWorld()
{
    PortageSettings *ps;
    return portageSettingsReloadWorld(ps);
}

const QStringList QPortageSettings::world()
{
    PortageSettings *ps;
    return QListConverter::StringListToQStringList(portageSettingsGetWorld(ps));
}

const QStringList QPortageSettings::archList()
{
    PortageSettings *ps;
    return QListConverter::StringListToQStringList(portageSettingsGetArchList(ps));

}

const QStringList QPortageSettings::virtuals()
{
    PortageSettings *ps;
    return QListConverter::StringListToQStringList(portageSettingsGetVirtuals(ps));
}

const QString QPortageSettings::acceptKeywords()
{
    PortageSettings *ps;
    return portageSettingsAcceptKeywords(ps);
}

const QStringList QPortageSettings::systemUseFlags()
{
    PortageSettings *ps;
    return QListConverter::StringListToQStringList(portageSettingsSystemUseFlags(ps));
}

const QString QPortageSettings::arch()
{
    PortageSettings *ps;
    return portageSettingsArch(ps);
}

const QString QPortageSettings::portdir()
{
    PortageSettings *ps;
    return portageSettingsPortdir(ps);
}

const QString QPortageSettings::portdirOverlay()
{
    PortageSettings *ps;
    return portageSettingsPortdirOverlay(ps);
}

const QString QPortageSettings::userConfigDir()
{
    PortageSettings *ps;
    return portageSettingsUserConfigDir(ps);
}

}//end namespace
